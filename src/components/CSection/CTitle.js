export default {
    name: "SectionTitle",
    // functional - превращает компонент в функциональный. Такие компоненты быстрее.
    // Подходит только для компонентов у которых нет реактивных свойств и экземпляра Vue (this)
    functional: true,
    // Render метод -  принимает саму функцию рендеринга первым параметром( h() ), вторым параметром прнимает обьект паарметров от родителя
    render(h, context) {
        return h(context.props.tag, { class: context.props.tClass }, context.children)
    },
    props: {
        tag: {
            type: String,
            default: 'span'
        },
        tClass: {
            type: String,
            default: 'section-title'
        }
    }
}
